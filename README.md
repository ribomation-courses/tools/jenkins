Jenkins CI/CD Server
====

Welcome to this course.
The syllabus can be find at
[tools/jenkins](https://www.ribomation.se/tools/jenkins.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to the sample project

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    git clone https://gitlab.com/ribomation-courses/tools/jenkins.git
    cd jenkins

Get the latest updates by a git pull operation

    git pull

Cloud Accounts
====

During the course we will use cloud services and you are advised to create accounts before course start.

GitLab
----

We are using this GIT service, because it provide private repositories for free. 

[Sign-up Here for GitLab](https://gitlab.com/users/sign_in)

Amazon Cloud Service (AWS)
----

We need a simple way to test SSH access. The AWS service requires you register a *credit card*. However, if you are new user and stick to the so called free tier, then it will not cost you dime for the exercises of the course. After the course you can just terminate your AWS account.

[Sign-up Here for AWS](https://aws.amazon.com/free/)


Installation Instructions
====

In order to do the programming exercises of the course, you need to have access to a Linux system, preferably Ubuntu. Go for one of the listed solutions below.

Although, Jenkins is written in Java and hence run on Windows as well, we insists of using Linux because that is the most common OS for running a jenkins server in production mode.

* **Already have Linux (or MacOS) installed on your laptop**<br/>
Then you are ready for the course. If it is not Ubuntu, such as Mac OSX, then there might be some differences, but as long as you can handle it and do the translation yourself, there is no problem.
* **Already have access to a remote Linux system**<br/>
Same as above.
* **Is running Windows 10 on your laptop**<br/>
One of the biggest news of the update named "Aniversary Edition" released in July 2016, was that Windows 10 has support for running native Ubuntu Linux, which is called WSL (Windows Subsystem for Linux). You just have to enabled it. Follow the links below to proceed.
* **Otherwise**<br/>
You have to install VirtualBox and install Ubuntu into a VM. Follow the links below to proceed.

In addition, you will need to install several build systems during the course.

Installing Ubuntu @ VBox
----

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop<br/>
    <http://www.ubuntu.com/download/desktop>
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
    Set a username and password when asked to and write them down.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>

Installing WSL @ Windows 10
----

1. How to Install and Use the Linux Bash Shell on Windows 10<br/>
    <https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/>
1. Once successfully installed WSL, you perhaps also want to have a handy right-click menu item of "Ubuntu Here"? Here is how to do it: 
    <http://winaero.com/blog/add-bash-to-the-folder-context-menu-in-windows-10/>
1. If you would like to launch graphical applications from WSL, i.e. X-Windows apps, then you need to install a X server for Windows, such as VcXsrv, Cygwin-X or similar. Inside WSL in your ~/.bashrc file, you need to define the DISPLAY environment variable:
    
    export DISPLAY=:0

* Windows 10's Bash shell can run graphical Linux applications with this trick<br/>
    <http://www.pcworld.com/article/3055403/windows/windows-10s-bash-shell-can-run-graphical-linux-applications-with-this-trick.html>
* VcXsrv Windows X Server<br/>
    <https://sourceforge.net/projects/vcxsrv/>

Links
====

For your conveience, we list below some links used during the course.

Sample Code
----

* https://github.com/ribomation/DroidAtScreen1
* https://gitlab.com/ribomation-jenkins-course/numbers-java-gradle
* https://gitlab.com/ribomation-jenkins-course/tag-cloud-cxx
* https://gitlab.com/ribomation-jenkins-course/numbers-web

Jenkins Info
----

* https://jenkins.io/
* https://jenkins.io/doc/
* https://wiki.jenkins-ci.org/display/JENKINS/Home
* https://wiki.jenkins-ci.org/display/JENKINS/Use+Jenkins
* https://plugins.jenkins.io/
* https://plugins.jenkins.io/deploy
* https://wiki.jenkins-ci.org/display/JENKINS/Plugins
* https://jenkins.io/doc/pipeline/steps/

Installation
----

* http://sdkman.io/
* https://gitlab.com/
* http://maven.apache.org/archetype/maven-archetype-plugin/examples/generate-batch.html
* http://maven.apache.org/archetypes/maven-archetype-quickstart/
* https://tomcat.apache.org/tomcat-8.0-doc/manager-howto.html
* http://www.jdev.it/deploying-your-war-file-from-jenkins-to-tomcat/
* http://nginx.org/en/docs/http/configuring_https_servers.html

AWS
----

* https://aws.amazon.com/
* https://aws.amazon.com/cli/
* https://cloud-images.ubuntu.com/locator/ec2/

GMail
----
* https://support.google.com/a/answer/176600?hl=en
* https://www.digitalocean.com/community/tutorials/how-to-use-google-s-smtp-server
* https://myaccount.google.com/lesssecureapps



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
